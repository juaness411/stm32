TABLE OF CONTENTS

               1. Project description 
               2. Technologies
               3 .Repository developer data

1. Project description

-This will be a temperature reading system.
-There will be two interfaces, AWS website,  and STM32's LCD.
-The LCD will show the current temperature  (every 5 seconds).
-The LCD will have a button that will turn  on/off a GPIO (anytime).
-The AWS website will show the current  temperature (every 30 seconds)

2. Technologies

ESP32:

-UART communication with the STM32  (UART parser).
-MQTT communication with AWS.

STM32F429I-DISC1:

-UART communication with ESP32 (UART parser).
-Sensor readings.
-Control LCD, and touch.
-Trigger actuators.

SENSOR BMP280:

-Read temperature.

3. Repository developer data

Juan Esteban Sarmiento Quintero
Email: juaness411@gmail.com
